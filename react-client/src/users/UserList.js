import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class UserList extends Component {
  state = {users: []}
  
  componentDidMount() {
    this.props.didMount(this)
  }
    
  render() {
    // https://goshakkk.name/controlled-vs-uncontrolled-inputs-react/
    return (
      <div>
        <h1>Users</h1>
        {this.state.users.map(user =>
          <div key={user.id}>Id: {user.id} - Handle: {user.handle}</div>
        )}        
        <p><Link to="/add-user-controlled">Add New User (controlled style)</Link></p>
        <p><Link to="/add-user-uncontrolled">Add New User (UNcontrolled style)</Link></p>
      </div>
    )
  }
}

export default UserList;
