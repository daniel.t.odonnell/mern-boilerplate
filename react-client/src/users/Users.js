import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import axios from 'axios';
import UserList from './UserList';
import AddUserControlled from './AddUserControlled';
import AddUserUncontrolled from './AddUserUncontrolled';

class Users extends Component {

  render() {
    return (
        <Switch>
            <Route path="/" exact={true} render={() => <UserList didMount={this.UserListDidMount} />} />
            <Route path="/add-user-controlled" render={(props) => <AddUserControlled {...props} onSubmit={this.AddUserControlledOnSubmit} />} />
            <Route path="/add-user-uncontrolled" render={(props) => <AddUserUncontrolled {...props} onSubmit={this.AddUserUncontrolledOnSubmit} />} />
        </Switch>
    );
  }

  UserListDidMount(context) {
    axios.get('/users')
      .then(res => context.setState({ users : res.data }))
      .catch(error => console.log(error));
  }
  AddUserControlledOnSubmit(context, newState) {
      console.log('yo')
    // setState is async and batched
    context.setState(newState);
    
    axios.post('/users', context.state)
      .then(res => context.props.history.push('/'))
      .catch(error => console.log(error));
  }

}

export default Users;
