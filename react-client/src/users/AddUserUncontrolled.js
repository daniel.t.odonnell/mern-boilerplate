import React, { Component } from 'react';
import axios from 'axios';

class AddUserUncontrolled extends Component {
  // "Uncontrolled" and "Stateless" style form submission - https://reactjs.org/docs/uncontrolled-components.html
  constructor(props) {
    super(props);

    // makes "this" and "this.state" available in events
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(evt) {
    console.log("componentDidMount()")
    this._id_ok.hidden     = "true";
    this._handle_ok.hidden = "true";
  }

  handleSubmit(evt) {
    let user = {};
    user.id = this._id.value;
    user.handle = this._handle.value;

    // validation
    let valid = true;

    if (!user.id || user.id.length === 0) {
      valid = false;
      this._id_ok.hidden="";
    }
    else {
      this._id_ok.hidden="true";
    }
    if (!user.handle || user.handle.length === 0) {
      valid = false;
      this._handle_ok.hidden="";      
    }
    else {
      this._handle_ok.hidden="true";      
    }

    if (valid) {   
      axios.post('/users', user)
        .then(res => this.props.history.push('/'))
        .catch(error => console.log(error));
    }

    evt.preventDefault();
  }
    
  render() {
    return (
      <form  onSubmit={this.handleSubmit}>
        <p><label>Id:<input 
          type="text" 
          name="id"
          ref={(ctl) => this._id = ctl} />
          <span ref={(ctl) => this._id_ok = ctl}>* required</span>
          </label></p>
        <p><label>Handle:<input 
          type="text" 
          name="handle" 
          ref={(ctl) => this._handle = ctl} />
          <span ref={(ctl) => this._handle_ok = ctl}>* required</span>
        </label></p>
        <input type="submit" value="Submit" />
      </form>
    )
  }
}

export default AddUserUncontrolled;
