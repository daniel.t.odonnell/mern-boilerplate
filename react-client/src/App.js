import React, { Component } from 'react';
import { BrowserRouter as Router} from 'react-router-dom';
import Users from './users/Users';
import './App.css';

class App extends Component {

  render() {
    return (
      <Router>
        <div className="App">
          <Users />
        </div>
      </Router>
    );
  }
}

export default App;
