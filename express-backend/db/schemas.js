const mongoose = require('./mongoose');

const userSchema = mongoose.Schema({
	id: String,
	handle: String
});

const schemas = {
	User : mongoose.model("user", userSchema)
}

module.exports = schemas;