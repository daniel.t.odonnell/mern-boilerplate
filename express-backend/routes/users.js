const express = require('express');
const router = express.Router();

const bodyParser = require('body-parser');
router.use(bodyParser.json()); // to support JSON-encoded bodies
router.use(bodyParser.urlencoded({extended: true })); 

const schemas = require('./../db/schemas');

/* GET users listing. */
router.get('/', (req, res, next) => {

  schemas.User.find({}, (err, data) => {
    if (err) { return next(err); }
    else {
      res.json(data);
    }
  });
});

// POST: Create new User
router.post('/', (req, res, next) => {
  
	// validate
	if (!req.body.id || req.body.id.length === 0) {
    return next(new Error("'id' field required"));
	}
	if (!req.body.handle || req.body.handle.length === 0) {
    return next(new Error("'handle' field required"));
	}

  let User = schemas.User;
  let newUser = new User();
  newUser.id = req.body.id;
  newUser.handle = req.body.handle;
  newUser.save((err, data) => {
    if (err) { return next(err); }
    else {
      res.json(newUser);
    }
  });
});

module.exports = router;